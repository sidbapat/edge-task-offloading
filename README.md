# Edge Task Offloading

* Surveyed various techniques and applications of Edge/Fog Computing, including Mobile Edge Computing to overcome the burden on central cloud servers and to meet the real-time application requirements.
* Produced a comparative analysis on various offloading techniques in Mobile Edge Computing and the various challenges faced by these techniques.
* Developed 6 clients and 3 servers using Java that successfully demonstrated the benefits of task offloading.

## Simulation Details

This simulation aims to demonstrate the logic behind task offloading in edge computing. It 
attempts to answer questions regarding as to why, what, and how task offloading is performed.
In the current times, portable devices are being used for various advanced applications such as 
video analysis, augmented realities, filters etc. Even though these devices grow more powerful 
each year, they are still limited in terms of resources. The application requirements are too 
intensive for the devices. Due to this, the devices are unable to run these applications efficiently 
without any energy or power loss. Due to their limitations, the results are delayed, and in some 
cases, these could have severe consequences. Therefore, there is a need to offload such heavy 
tasks to nearby servers that are better equipped to handle heavy tasks. By offloading tasks to 
nearby servers whenever necessary, energy is conserved at the devices, thus extending their 
battery lives and the results can be obtained quickly for time-critical applications.

This simulation consists of 3 edge servers and at most 6 clients. More clients and servers can 
be added but certain modifications will be needed in the code for it to work just as same. The 
new servers must have new IP addresses that need to be manually updated in each of the client’s 
server list. Other values unique to a client/server will also need to be updated for the new clients 
or servers but the main code is the same for all clients and all servers respectively. For this 
simulation, the task is to calculate the range between the largest and smallest numbers from a 
list of numbers provided by the user from the console (System.in). This is followed by an image 
processing task where the client merely receives a .jpg file from the user and it is sent to the 
server which processes the image and creates another .jpg copy of that image. For this 
simulation, the copy of the image under the name “clientX.jpg” (X is the client number) is 
created and placed in the desktop.

The following will need 
to be modified whenever new clients or servers are added.
#### Client:
If new servers are added, new IP addresses must be provided as appropriate and include them 
in the following function in each of the clients. Currently there are 3 servers. There is no dynamic method to automatically include new servers so this will need to be done manually 
prior to execution.
```
Public void initialise_servers(){
serverlist.add("127.0.0.1");
serverlist.add("127.0.0.2");
serverlist.add("127.0.0.3");
}
```
#### Server:
1) If you add a new server, modify the address as follows. Currently there are 3 servers 
with addresses 127.0.0.1, 127.0.0.2, 127.0.0.3. Other servers must have different 
addresses.
```
String address="127.0.0.1";
```
2) Modify the following as appropriate to reflect the server. In this case, the last digit of 
the address represents the server, i.e. 127.0.0.1 is Server 1, 127.0.0.2 is Server 2 and so 
on.
```
System.out.println("---------------------------------------------------------------------");
System.out.println("Server 1 Available");
System.out.println("---------------------------------------------------------------------")
```

## Execution Instructions

I used Java for this demonstration and all the communications were done using java socket 
programming with input and output streams. The execution was performed using IntelliJ. The 
following was executed with 3 edge servers and different number of clients each time.

1. Run all the edge servers.

2. Run all the client programs. Make sure the list of numbers provided are separated by 
commas and without any space. Type in the number list and click Enter to 
begin the execution. You must provide the pathname of the .jpg file as the main function 
argument. In IntelliJ it must be provided as program arguments in the “edit 
configurations” drop down option for the respective client program. If execution is done 
via the terminal, merely type in the pathname after the execution command.

3. The execution requirements for local execution and for each of the servers are 
determined. The time and energy requirements are determined and a satisfaction function value is obtained.


    For example, suppose the satisfaction value is the lowest for Server 1, 
    the task is offloaded to Server 1 in the string form as mentioned earlier. The server 
    executed the task and returned the result back to the client which is displayed in the client console. The result is the range between the  smallest 
    number and the largest number. The .jpg file is also passed to the server which 
    processes it and creates a copy. In this simulation, the server creates another .jpg file in 
    your system’s desktop as clientX.jpg where X is the client number.
    The same server is available once again for the next client, i.e. the next connection 
    request.Similarly, the above occurs for the other servers whenever the task is to be offloaded to 
    the other servers as appropriate.

    In case the task is to be executed locally, no offloading occurs and the results are 
    computed locally and displayed in the client console.

    For example, suppose the client runs for 23,45,67,32,78 as the input. The local requirements had the 
    lowest satisfaction value so the task was executed in the client itself.
    Even though local execution in general isn’t very efficient, it could turn out to be the 
    most efficient execution in the worst-case scenario. Image processing task is not 
    performed for local execution.


### NOTE:
The formulae for time and energy requirements and satisfaction function were inspired by the paper below:

Yuben Qu and Jiajia Liu. 2019. Computation offloading for mobile edge computing with accuracy guarantee. In <i>Proceedings of the ACM Turing Celebration Conference - China</i> (<i>ACM TURC '19</i>). Association for Computing Machinery, New York, NY, USA, Article 21, 1–5.
